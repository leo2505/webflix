import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FilmesEditar extends StatefulWidget {

  final String tipoEdicao;
  final DocumentSnapshot dadosFilme;
  FilmesEditar(this.tipoEdicao, this.dadosFilme);

  @override
  _FilmesEditarState createState() => _FilmesEditarState();
}

class _FilmesEditarState extends State<FilmesEditar> {



  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController nomeFilme = TextEditingController();
  TextEditingController precoFilme = TextEditingController();
  TextEditingController idGenero = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.tipoEdicao=="alt"){
      nomeFilme.text = widget.dadosFilme.data["nomeFilme"].toString();
      precoFilme.text = widget.dadosFilme.data["precoFilme"].toString();
      idGenero.text = widget.dadosFilme.data["idGenero"].toString();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.tipoEdicao =='inc' ? "Inclusão de Filmes": "Alteração de Filmes", style: TextStyle(
            color: Colors.red,
          )),
          backgroundColor: Colors.black,
        ),
        backgroundColor: Colors.black,
        body: editar());
  }

  editar() {
    return SingleChildScrollView(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Container(
        color: Colors.black,
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      labelText: "Nome Filme",
                      labelStyle: TextStyle(
                          color: Colors.white70,
                          fontSize: 18,
                      ),
                  ),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.left,
                  controller: nomeFilme,
                  validator: (value){
                    if(value.isEmpty){
                      return "Informe o nome do Filme";
                    }
                    return null;
                  }
              ),
              TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      labelText: "Preço Filme",
                      labelStyle: TextStyle(color: Colors.white70, fontSize: 18)
                  ),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.left,
                  controller: precoFilme,
                  validator: (value){
                    if(value.isEmpty){
                      return "Informe o preço do Filme";
                    }
                    return null;
                  }
              ),
              TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      labelText: "ID Gênero",
                      labelStyle: TextStyle(color: Colors.white70, fontSize: 18)
                  ),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.left,
                  controller: idGenero,
                  validator: (value){
                    if(value.isEmpty){
                      return "Informe ID do gênero do filme";
                    }
                    return null;
                  }
              ),
              RaisedButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text("Salvar"),
                onPressed: (){
                  if(widget.tipoEdicao=="inc"){
                    Firestore.instance.collection("filmes").add(
                        {
                          "nomeFilme": nomeFilme.text,
                          "precoFilme": precoFilme.text,
                          "idGenero": idGenero.text
                        }
                    );
                  } else{

                    Firestore.instance.collection("filmes")
                    .document(widget.dadosFilme.documentID)
                        .updateData(
                        {
                          "nomeFilme": nomeFilme.text,
                          "precoFilme": precoFilme.text,
                          "idGenero": idGenero.text
                        }
                    );
                  }

                  Navigator.pop(context);
                },
              )
            ],
          ),
        ),
      )
    );
  }
}
