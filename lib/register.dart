import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController userEmail = TextEditingController();
  TextEditingController userPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
   return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Login",
          style: TextStyle(
            color: Colors.red,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: register(),
    );
  }

  register(){
    return SingleChildScrollView(
      padding:  EdgeInsets.all(10),
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
              style: TextStyle(color: Colors.white),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  icon: Icon(Icons.email, color: Colors.red,),
                  labelText: "Email", labelStyle: TextStyle(color: Colors.white)),
              controller: userEmail,
              validator: (valor){
                if(valor.isEmpty){
                  return "Informe o email";
                } else if (!userEmail.text.contains("@")){
                  return "Informe um email válido";
                }
                return null;
              },
            ),
            TextFormField(
              style: TextStyle(color: Colors.white),
              keyboardType: TextInputType.text,
              obscureText: true,
              decoration: InputDecoration(
                  icon: Icon(Icons.lock, color: Colors.red,),
                  labelText: "Senha", labelStyle: TextStyle(color: Colors.white)),
              controller: userPassword,
              validator: (valor){
                if(valor.isEmpty){
                  return "Senha inválida";
                }
                return null;
              },
            ),
            RaisedButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text("Cadastrar"),
                onPressed: (){
                  if(formKey.currentState.validate()){
                    validating();
                  }
                }
            ),
          ],
        ),
      ),
    );
  }
  Future<void> validating() async{
    loading();
    try{
      FirebaseUser usuario = await FirebaseAuth.instance.
      createUserWithEmailAndPassword(
          email: userEmail.text, password: userPassword.text
      );
      Firestore.instance.collection("usuarios").document(usuario.uid).setData(
        {
          "nivelUser": 0
        }
      );
      Navigator.pop(context);
      Navigator.pop(context);
//      Navigator.push(context, MaterialPageRoute(builder: (context) => homePost()));
    } catch(error){
      Navigator.pop(context);
      SnackBar snackBar = SnackBar(
        backgroundColor: Colors.amberAccent,
        content:
//        Text ("Erro ao cadastrar", style: TextStyle(fontSize: 14, color: Colors.black),),
          Text (error.toString(), style: TextStyle(fontSize: 14, color: Colors.black),),
//           Text (error.tString(), style: TextStyle(fontSize: 10, color: Colors.black),),

      );
      scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
  void loading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Color(0),
          child: new Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
            padding: EdgeInsets.all(10),
            height: 70,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                new CircularProgressIndicator(),
                SizedBox(
                  width: 30,
                ),
                new Text(" Verificando ..."),
              ],
            ),
          ),
        );
      },
    );
  }
}
