import 'package:app_firebase/filmes_editar.dart';
import 'package:app_firebase/recovery.dart';
import 'package:app_firebase/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'filmes.dart';

void main() {
//  Firestore.instance.collection("filmes").document().setData(
//      {
//        "nomeFilme": "Coringa",
//        "precoFilme": "90.99",
//        "idGenero": 3
//      }
//  );
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController userEmail = TextEditingController();
  TextEditingController userPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(
            "Login",
            style: TextStyle(
              color: Colors.red,
            ),
          ),
          backgroundColor: Colors.black,
        ),
        backgroundColor: Colors.black,
        body: login(),
    );
  }

  login(){
    return SingleChildScrollView(
      padding:  EdgeInsets.all(10),
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
              style: TextStyle(color: Colors.white),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  icon: Icon(Icons.email, color: Colors.red,),
                  labelText: "Email", labelStyle: TextStyle(color: Colors.white)),
              controller: userEmail,
              validator: (valor){
                if(valor.isEmpty){
                  return "Informe o email";
                } else if (!userEmail.text.contains("@")){
                  return "Informe um email válido";
                }
                return null;
              },
            ),
            TextFormField(
              style: TextStyle(color: Colors.white),
              keyboardType: TextInputType.text,
              obscureText: true,
              decoration: InputDecoration(
                  icon: Icon(Icons.lock, color: Colors.red,),
                  labelText: "Senha", labelStyle: TextStyle(color: Colors.white)),
              controller: userPassword,
              validator: (valor){
                if(valor.isEmpty){
                  return "Senha inválida";
                }
                return null;
              },
            ),
            RaisedButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text("Entrar"),
                onPressed: (){
                  if(formKey.currentState.validate()){
                    validating();
                  }
                }
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  textColor: Colors.white70,
                  child: Text("Esqueci a senha", textAlign: TextAlign.center,),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Recovery()));
                  },
                ),
                FlatButton(
                  textColor: Colors.white70,
                  child: Text("Registrar", textAlign: TextAlign.center,),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Register()));
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
  Future<void> validating() async{
    loading();
    try{
      FirebaseUser user = await FirebaseAuth.instance.
      signInWithEmailAndPassword(
        email: userEmail.text, password: userPassword.text
      );
      /* Para filtrar apenas do usuário */
      DocumentSnapshot dadosUsuario = await Firestore.instance.collection("usuarios").document(user.uid).get();
      Navigator.pop(context);
      Navigator.push(context, MaterialPageRoute(builder: (context) => homePost()));
    } catch(error){
      Navigator.pop(context);
      SnackBar snackBar = SnackBar(
        backgroundColor: Colors.amberAccent,
        content:
        Text ("Erro ao fazer login", style: TextStyle(fontSize: 14, color: Colors.black),),
//           Text ("Erro ao fazer login\n" + error.toString(), style: TextStyle(fontSize: 14, color: Colors.black),),
//           Text (error.tString(), style: TextStyle(fontSize: 10, color: Colors.black),),

      );
      scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
  void loading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Color(0),
          child: new Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
            padding: EdgeInsets.all(10),
            height: 70,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                new CircularProgressIndicator(),
                SizedBox(
                  width: 30,
                ),
                new Text(" Verificando ..."),
              ],
            ),
          ),
        );
      },
    );
  }
  homePost(){
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "WebFlix",
          style: TextStyle(
            color: Colors.red,
          ),
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: home(),
    );
  }
  home() {
    return Container(
      color: Colors.black,
      child: Column(
        children: <Widget>[
          RaisedButton(
            child: Text("Filmes"),
            onPressed: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Filmes())
              );
            },
          )
        ],
      )
    );
  }

}
