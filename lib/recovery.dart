import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Recovery extends StatefulWidget {
  @override
  _RecoveryState createState() => _RecoveryState();
}

class _RecoveryState extends State<Recovery> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController userEmail = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("Recuperar Senha"),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: rec()
    );
  }
  rec(){
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
              style: TextStyle(color: Colors.white),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  icon: Icon(Icons.email, color: Colors.red,),
                  labelText: "Email", labelStyle: TextStyle(color: Colors.white)),
              controller: userEmail,
              validator: (valor){
                if(valor.isEmpty){
                  return "Informe o email";
                } else if (!userEmail.text.contains("@")){
                  return "Informe um email válido";
                }
                return null;
              },
            ),
            RaisedButton(
              color: Colors.red,
              textColor: Colors.white,
              child: Text("Requisiar Senha Nova"),
              onPressed: (){
                if(formKey.currentState.validate()){
                  registrar();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
  Future<void> registrar() async{
    try{
      await FirebaseAuth.instance.
        sendPasswordResetEmail(email: userEmail.text);
      Navigator.pop(context);
    } catch(error){
      SnackBar snackBar = SnackBar(
        backgroundColor: Colors.amberAccent,
        content: Text("Erro ao verificar o email")
      );
      scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}
